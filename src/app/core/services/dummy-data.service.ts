import {Injectable} from '@angular/core';
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";


@Injectable()
export class DummyDataService{

  constructor() {

  }

  getLoginData():Object {      //todo find a way to delay the response by 1 sec.
    return {
      "user": {
        "name": "Topsy Kret",
        "roles": ['OfflineUser','Admin'],
        "email": "qqq@bnh.com",
        "authToken": "11111.OFFLINETOKEN_!!!!!",
      }
    }
  }

  getListData():Object {
    return ['a','b','c','d'];
  }

  getDeviceData():Object {
    return [
      {
        type:'dummy type 1',
        model:'dummy model 1'
      },
      {
        type:'dummy type 2',
        model:'dummy model 2'
      }
    ];
  }

}
