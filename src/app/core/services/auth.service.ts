import { Injectable } from '@angular/core';
import {LocalStorageService} from "./local-storage.service";
import {Router} from "@angular/router";

@Injectable()
export class AuthService {

  constructor(
    private localStorage:LocalStorageService,
    private router:Router
  ) {
    console.log("creating the AUTH SERVICE");
  }

  logout(){
    this.isLoggedIn = false;
    this.localStorage.delete('userAuthData');     // clean up from local storage
    this.router.navigate(['/login']);
  }

  public isLoggedIn:boolean = false;
  public user:any;

}


