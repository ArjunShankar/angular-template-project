import { NgModule, Optional, SkipSelf  } from '@angular/core';
import {CommonModule} from "@angular/common";
import {AuthService} from "./services/auth.service";
import {DummyDataService} from "./services/dummy-data.service";
import {HttpHeadersService} from "./services/http-headers.service";
import {LocalStorageService} from "./services/local-storage.service";
import {AuthGuard} from "./services/auth.guard";
import {ConfigService} from "./services/config.service";
import { APP_INITIALIZER } from '@angular/core';
import { throwIfAlreadyLoaded } from './services/module-import-guard';


export function initConfig(config: ConfigService){     // recent change. Earlier this was a lambda expression within useFactory
  return () => config.load()
  //return config.load()
}


@NgModule({
  imports: [
    CommonModule
  ],
  providers:[
    AuthService,            //maintains session,userinfo
    DummyDataService,       //for offline feature
    HttpHeadersService,     //creates http-headers
    LocalStorageService,    //get-set data from local storage
    AuthGuard,              //checks if session valid or not on routing
    ConfigService,          //read config file
    { provide: APP_INITIALIZER, useFactory: initConfig, deps: [ConfigService], multi: true }  // makes sure that config file is read before anything  else
  ],

  declarations: []
})



export class CoreModule {
 // constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
 //   throwIfAlreadyLoaded(parentModule, 'CoreModule');
 // }
}


