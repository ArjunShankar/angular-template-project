import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MergedItemComponent } from './merged-item.component';

describe('MergedItemComponent', () => {
  let component: MergedItemComponent;
  let fixture: ComponentFixture<MergedItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MergedItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergedItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
