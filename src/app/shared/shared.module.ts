import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {NgxChartsModule} from "@swimlane/ngx-charts";
import {SimpleButton} from "./components/button";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { MergedItemComponent } from './components/merged-item/merged-item.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    HttpModule,
    NgxChartsModule
  ],
  exports: [
    FormsModule,
    HttpModule,
    SimpleButton,
    MaterialModule,
    NgxChartsModule
  ],
  declarations: [
    SimpleButton,
    MergedItemComponent
  ]
})
export class SharedModule { }
