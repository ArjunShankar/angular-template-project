/**
 * Created by arjun on 25/3/17.
 */
export class User {
  roles:Array<string>;
  name: string;
  email:string;
  authToken:string;
}
