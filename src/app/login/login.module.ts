import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegisterUserComponent } from './register-user/register-user.component';
import {RouterModule} from "@angular/router";
import {SharedModule} from "../shared/shared.module";
import {LoginService} from "./services/login.service";

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      { path: '', component: LoginComponent }
    ])
  ],
  providers:[LoginService],
  declarations: [LoginComponent, RegisterUserComponent]
})
export class LoginModule { }
