/**
 * Created by arjun on 25/3/17.
 */

export class LoginRequestVO{
  private username:string;
  private password:string;

  constructor(userName:string,password:string){
    this.username = userName;
    this.password = password;
  }
}
