/**
 * Created by arjun on 25/3/17.
 */
import { Injectable } from '@angular/core';
import { Http, Response, RequestOptionsArgs} from '@angular/http';

import {Observable, Subscriber} from 'rxjs/Rx';

import {LoginResponseVO} from "../models/LoginResponseVO";
import {LoginRequestVO} from "../models/LoginRequestVO";
import {ConfigService} from "../../core/services/config.service";
import {HttpHeadersService} from "../../core/services/http-headers.service";
import {DummyDataService} from "../../core/services/dummy-data.service";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class LoginService{

  constructor(private http: Http,
              private config:ConfigService,
              private headerService:HttpHeadersService,
              private dummyDataService:DummyDataService) {

  }

  login(loginVO:LoginRequestVO): Observable<any[]> {
    let body = JSON.stringify(loginVO);
    let options: RequestOptionsArgs = <RequestOptionsArgs>{};
    let remoteURL:string = this.config.CONFIG['urls'];

    options.url = remoteURL['server'] + 'login';

    options.headers = this.headerService.unsecuredHeaders;   // use headers.append(key,value) to make any changes here.

    if(this.config.CONFIG['offlineMode']){  // to be used when in offline mode.

      let offlineLoginResponse:Object = this.dummyDataService.getLoginData();
      return new Observable<any>((subscriber: Subscriber<any>) => subscriber.next(offlineLoginResponse))
        .map(res => res);

    }
    else{
      return this.http.post(``, body, options)
        .map((r: Response) => r.json() as LoginResponseVO)
        .catch(this.handleObservableError);
    }
  }

  validateSession(existingSession:any): Observable<any[]> {
    let body = JSON.parse(existingSession);
    let options: RequestOptionsArgs = <RequestOptionsArgs>{};
    let remoteURL:string = this.config.CONFIG['urls'];

    options.url = remoteURL['server'] + 'validateSession';

    options.headers = this.headerService.unsecuredHeaders;

    if(this.config.CONFIG['offlineMode']){  // to be used when in offline mode.

      let offlineLoginResponse:Object = this.dummyDataService.getLoginData();
      return new Observable<any>((subscriber: Subscriber<any>) => subscriber.next(offlineLoginResponse))
        .map(res => res);
    }
    else{
      return this.http.post(``, body, options)
        .map((r: Response) => r.json() as LoginResponseVO)
        .catch(this.handleObservableError);
    }
  }

  handleObservableError(error:any){
    return Observable.throw(error.json().error || 'The cats pulled the power cord....');
  }

}
