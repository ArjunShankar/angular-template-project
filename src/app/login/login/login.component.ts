import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";
import {ConfigService} from "../../core/services/config.service";
import {AuthService} from "../../core/services/auth.service";
import {LocalStorageService} from "../../core/services/local-storage.service";
import {Router} from "@angular/router";
import {LoginService} from "../services/login.service";
import {User} from "../../shared/models/User";
import {LoginRequestVO} from "../models/LoginRequestVO";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private authService:AuthService,
    private loginService:LoginService,
    private router:Router,
    private config:ConfigService,
    private localStorage:LocalStorageService
  ) {

  }


  ngOnInit() {
    console.log("checking for prev session")
    let prevSession:Object = this.localStorage.getItem('userAuthData');

    if(prevSession !== null){
      console.log("prev session found",prevSession)
      this.loginService.validateSession(prevSession)
        .subscribe(
          (receivedData: any) => {
            this.authService.user = receivedData as User;
            this.authService.isLoggedIn = true;
            this.router.navigate(['/dashboard']);         //TODO we should maintain wherever user originally wanted to go..instead of dumping on dashboard
          },
          (errorMsg:any) =>{
            window.alert(errorMsg);
          }
        )
    }
    else{
      console.log("no prev session found")
    }
  }

  private formInput = {
    username:'',
    password:''
  }

  onLoginFormSubmit(input:NgForm){
    this.login();
  }

  login(){
    let loginRequestVO:LoginRequestVO = new LoginRequestVO(this.formInput.username,this.formInput.password);
    this.loginService.login(loginRequestVO)
      .subscribe(
        (receivedData: any) => {
          this.authService.user = receivedData as User;
          this.authService.isLoggedIn = true;
          //  this.authService.authToken = receivedData.authToken;
          this.localStorage.setItem('userAuthData',JSON.stringify(this.authService.user));
          //this.localStorage.setItem('authToken',this.authService.authToken);
          this.router.navigate(['/dashboard']);
        },
        (errorMsg:string) =>{
          window.alert(errorMsg);
        })

  }

}
