import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NotFoundComponent} from "./not-found/not-found.component";


const appRoutes: Routes = [
  { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule'},
  { path: 'login', loadChildren: './login/login.module#LoginModule' },
  { path: 'admin',loadChildren: './admin/admin.module#AdminModule'},
  { path: 'pages', loadChildren: './pages/pages.module#PagesModule' },
  { path: 'devices', loadChildren: './devices/devices.module#DevicesModule'},
  //{ path: 'sites', loadChildren: '../sites/sites.module#SitesModule' },
  { path: '', redirectTo: '/login',pathMatch:'full' },
  { path: '**', component: NotFoundComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(appRoutes,{useHash:true})],
  exports: [RouterModule],
})
export class AppRoutingModule { }
