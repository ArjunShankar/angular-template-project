import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './components/layout/layout.component';
import { Page1Component } from './components/page1/page1.component';
import { Page2Component } from './components/page2/page2.component';
import {PagesRoutingModule} from "./pages.routing";

@NgModule({
  imports: [
    CommonModule,
    PagesRoutingModule
  ],
  declarations: [LayoutComponent, Page1Component, Page2Component]
})
export class PagesModule {

}
