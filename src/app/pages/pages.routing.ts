/**
 * Created by arjun on 4/4/17.
 */
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './components/layout/layout.component';
import {Page1Component} from "./components/page1/page1.component";
import {Page2Component} from "./components/page2/page2.component";



const childRoutes: Routes = [
  { path: '',component:LayoutComponent,
    children:[
      {path:'page1',component:Page1Component},
      {path:'page2',component:Page2Component},
    ]},

];


@NgModule({
  imports: [
    RouterModule.forChild(childRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class PagesRoutingModule {

}
