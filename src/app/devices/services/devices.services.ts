/**
 * Created by arjun on 26/3/17.
 */
import { Injectable } from '@angular/core';
import {Http, Response, RequestOptionsArgs} from '@angular/http';

import 'rxjs/add/operator/toPromise';

import {Observable, Subscriber} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {ConfigService} from "../../core/services/config.service";
import {HttpHeadersService} from "../../core/services/http-headers.service";
import {DummyDataService} from "../../core/services/dummy-data.service";

@Injectable()
export class DeviceService{

  constructor(private http: Http,
              private config:ConfigService,
              private dummyDataService:DummyDataService,
              private headerService:HttpHeadersService) {

  }

  getDeviceData(): Observable<any[]> {
    let options: RequestOptionsArgs = <RequestOptionsArgs>{};
    if(this.config.CONFIG['offlineMode']){  // to be used when in offline mode.
      let offlineLoginResponse:Object = this.dummyDataService.getDeviceData();
      return new Observable<any>((subscriber: Subscriber<any>) => subscriber.next(offlineLoginResponse))
        .map(res => res);
    }
    else{
      let remoteURL:string = this.config.CONFIG['urls'];
      options.url = remoteURL['server'] +remoteURL['apiPrefix'] + '/' + 'devices';
      options.headers = this.headerService.headers;
      return this.http.get(``, options)
        .map((r: Response) => r.json())
        .catch(this.handleObservableError);
    }
  }

  handleObservableError(error:any){
    return Observable.throw(error.json().error || 'Unable to get list of devices.');
  }

}
