import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeviceComponent } from './components/device/device.component';
import {DeviceRouterModule} from "./devices.routing";

@NgModule({
  imports: [
    CommonModule,
    DeviceRouterModule
  ],
  declarations: [DeviceComponent]
})
export class DevicesModule { }
