

import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {AdminConsoleComponent} from "./admin-console/admin-console.component";


@NgModule({
  imports:[RouterModule.forChild([
    {path: '', component: AdminConsoleComponent }
  ])],
  exports:[RouterModule]
})

export class AdminRouterModule{

}
