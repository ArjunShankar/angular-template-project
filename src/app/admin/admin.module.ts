import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminConsoleComponent } from './admin-console/admin-console.component';
import {AdminRouterModule} from "./admin.routing";
import {AdminService} from "./services/admin.service";
import {SharedModule} from "../shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AdminRouterModule
  ],
  providers:[AdminService],
  declarations: [AdminConsoleComponent]
})
export class AdminModule { }
