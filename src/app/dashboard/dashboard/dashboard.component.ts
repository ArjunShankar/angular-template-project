import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../core/services/auth.service";
import {ConfigService} from "../../core/services/config.service";
import {Router} from "@angular/router";
import {DashboardService} from "../services/dashboard.service";
import {AdminService} from "../../admin/services/admin.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(
    private authService:AuthService,
    private configService:ConfigService,
    private dashboardService:DashboardService,
    private adminService:AdminService,
    private router:Router
  ) {

  }
   single = [
  {
    "name": "Germany",
    "value": 8940000
  },
  {
    "name": "USA",
    "value": 5000000
  },
  {
    "name": "France",
    "value": 7200000
  }
];

  multi = [
  {
    "name": "Germany",
    "series": [
      {
        "name": "2006",
        "value":2000
      },
      {
        "name": "2007",
        "value": 60002
      },
      {
        "name": "2008",
        "value": 500
      },
      {
        "name": "2009",
        "value": 50002
      },
      {
        "name": "2010",
        "value": 5000002
      },
      {
        "name": "2011",
        "value": 5800000
      }
    ]
  },

  {
    "name": "USA",
    "series": [
      {
        "name": "2006",
        "value":20000
      },
      {
        "name": "2007",
        "value": 6000002
      },
      {
        "name": "2008",
        "value": 610000
      },
      {
        "name": "2009",
        "value": 500002
      },
      {
        "name": "2010",
        "value": 5000002
      },
      {
        "name": "2011",
        "value": 5800000
      }
    ]
  },

  {
    "name": "France",
    "series": [
      {
        "name": "2006",
        "value":200
      },
      {
        "name": "2007",
        "value": 6000002
      },
      {
        "name": "2008",
        "value": 500000
      },
      {
        "name": "2009",
        "value": 500002
      },
      {
        "name": "2010",
        "value": 5000002
      },
      {
        "name": "2011",
        "value": 5800000
      }
    ]
  }
];


  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Population';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };


  ngOnInit() {
  }

  onSelect(evt){
    console.log("chart event:::" +evt);
  }




  logout(){
    this.authService.logout();
    this.navigateTo('login');
   // this.router.navigate(['/login']);   // this.router.navigate(['/login', login.id]);
  }
  navigateTo(link){
    this.router.navigate([link]);
  }

  links = ['dashboard','devices','sites'];
t

}
