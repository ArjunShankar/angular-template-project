import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import {DashboardRoutingModule} from "./dashboard.routing";
import {SharedModule} from "../shared/shared.module";
import {DashboardService} from "./services/dashboard.service";
import {AdminModule} from "../admin/admin.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";




@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    AdminModule
  ],
  providers:[DashboardService],
  declarations: [DashboardComponent]
})
export class DashboardModule { }
