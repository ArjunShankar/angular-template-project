Angular-template-project

Made using angular-cli.

I am upgrading my earlier angular2-template-project(https://gitlab.com/FullMetal/angular2-template-project) to Angular4.  
This will have all the features from the previous project, the only difference being that the new project will be built using angular-cli.

Keeping up with the accepted vernacular... now it is just Angular(Not Angular2/4).

Will update readme as I add the features. The old backend should work as it is.

Will try to use some of the Angular(4) features.

To know what's new in Angular(4), check the following link...
https://twitter.com/stephenfluin/status/841938081091584001

Update(21 March 2017):
Going for a newer approach, because we didn't explore into child routes in the previous app.

The new app structure will be

APP
 |
 |---> Login Module
 |
 |---> Admin Module
 |
 |---> Layout Module  ------------------------------> Sites Module
 |                                            |---->  Device Module
 |
 |---> Dashboard Module


 Going this way because Sites and Device pages will share the same basic layout. It will be stupid to duplicate the layout and will give a chance to learn child routes.
